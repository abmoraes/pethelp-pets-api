﻿// <auto-generated />
using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using Pethelp.Pets.Infrastructure.Repository;

namespace Pethelp.Pets.Infrastructure.Migrations
{
    [DbContext(typeof(PetsDbContext))]
    partial class PetsDbContextModelSnapshot : ModelSnapshot
    {
        protected override void BuildModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("Relational:MaxIdentifierLength", 64)
                .HasAnnotation("ProductVersion", "5.0.12");

            modelBuilder.Entity("Pethelp.Pets.Domain.Model.Pet", b =>
                {
                    b.Property<int>("PetId")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    b.Property<string>("Breed")
                        .HasColumnType("varchar(50)");

                    b.Property<bool>("Castrated")
                        .HasColumnType("tinyint(1)");

                    b.Property<string>("Color")
                        .HasColumnType("varchar(20)");

                    b.Property<DateTime>("CreatedAt")
                        .HasColumnType("datetime(6)");

                    b.Property<DateTime>("DateBirth")
                        .HasColumnType("Date");

                    b.Property<string>("Gender")
                        .HasMaxLength(30)
                        .HasColumnType("varchar(30)");

                    b.Property<string>("Name")
                        .HasColumnType("varchar(20)");

                    b.Property<string>("Size")
                        .HasColumnType("varchar(20)");

                    b.Property<string>("Type")
                        .HasColumnType("varchar(20)");

                    b.Property<DateTime>("UpdatedAt")
                        .HasColumnType("datetime(6)");

                    b.HasKey("PetId");

                    b.ToTable("Pets");
                });

            modelBuilder.Entity("Pethelp.Pets.Domain.Model.PetPhotos", b =>
                {
                    b.Property<int>("PhotoId")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    b.Property<string>("BucketName")
                        .HasColumnType("varchar(50)");

                    b.Property<DateTime>("CreatedAt")
                        .HasColumnType("datetime(6)");

                    b.Property<string>("Desc")
                        .HasColumnType("varchar(50)");

                    b.Property<string>("Key")
                        .HasColumnType("longtext");

                    b.Property<int?>("PetId")
                        .HasColumnType("int");

                    b.HasKey("PhotoId");

                    b.HasIndex("PetId");

                    b.ToTable("PetPhotos");
                });

            modelBuilder.Entity("Pethelp.Pets.Domain.Model.PetPost", b =>
                {
                    b.Property<int>("PostId")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    b.Property<DateTime>("CreatedAt")
                        .HasColumnType("datetime(6)");

                    b.Property<DateTime>("DateOccurrence")
                        .HasColumnType("datetime(6)");

                    b.Property<string>("Desc")
                        .HasColumnType("longtext");

                    b.Property<int?>("PetId")
                        .HasColumnType("int");

                    b.Property<string>("Phone")
                        .HasColumnType("varchar(11)");

                    b.Property<int?>("PostAddressAddressId")
                        .HasColumnType("int");

                    b.Property<string>("Status")
                        .HasColumnType("varchar(50)");

                    b.Property<DateTime>("UpdatedAt")
                        .HasColumnType("datetime(6)");

                    b.Property<int>("UserId")
                        .HasColumnType("int");

                    b.HasKey("PostId");

                    b.HasIndex("PetId");

                    b.HasIndex("PostAddressAddressId");

                    b.ToTable("PetPost");
                });

            modelBuilder.Entity("Pethelp.Pets.Domain.Model.PostAddress", b =>
                {
                    b.Property<int>("AddressId")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    b.Property<string>("Address")
                        .HasColumnType("varchar(50)");

                    b.Property<string>("City")
                        .HasColumnType("varchar(50)");

                    b.Property<string>("Country")
                        .HasColumnType("varchar(50)");

                    b.Property<string>("Neighborhood")
                        .HasColumnType("varchar(50)");

                    b.Property<string>("Number")
                        .HasColumnType("varchar(6)");

                    b.Property<string>("PostalCode")
                        .HasColumnType("varchar(8)");

                    b.Property<string>("State")
                        .HasColumnType("varchar(50)");

                    b.HasKey("AddressId");

                    b.ToTable("PostAddress");
                });

            modelBuilder.Entity("Pethelp.Pets.Domain.Model.PetPhotos", b =>
                {
                    b.HasOne("Pethelp.Pets.Domain.Model.Pet", null)
                        .WithMany("Photos")
                        .HasForeignKey("PetId");
                });

            modelBuilder.Entity("Pethelp.Pets.Domain.Model.PetPost", b =>
                {
                    b.HasOne("Pethelp.Pets.Domain.Model.Pet", "Pet")
                        .WithMany()
                        .HasForeignKey("PetId");

                    b.HasOne("Pethelp.Pets.Domain.Model.PostAddress", "PostAddress")
                        .WithMany()
                        .HasForeignKey("PostAddressAddressId");

                    b.Navigation("Pet");

                    b.Navigation("PostAddress");
                });

            modelBuilder.Entity("Pethelp.Pets.Domain.Model.Pet", b =>
                {
                    b.Navigation("Photos");
                });
#pragma warning restore 612, 618
        }
    }
}
