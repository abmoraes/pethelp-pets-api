﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Pethelp.Pets.Infrastructure.Migrations
{
    public partial class AddGenderField : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_PetPhotos_PetPost_PetPostPostId",
                table: "PetPhotos");

            migrationBuilder.DropIndex(
                name: "IX_PetPhotos_PetPostPostId",
                table: "PetPhotos");

            migrationBuilder.DropColumn(
                name: "PetPostPostId",
                table: "PetPhotos");

            migrationBuilder.AddColumn<string>(
                name: "Gender",
                table: "Pets",
                type: "varchar(30)",
                maxLength: 30,
                nullable: true)
                .Annotation("MySql:CharSet", "utf8mb4");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Gender",
                table: "Pets");

            migrationBuilder.AddColumn<int>(
                name: "PetPostPostId",
                table: "PetPhotos",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_PetPhotos_PetPostPostId",
                table: "PetPhotos",
                column: "PetPostPostId");

            migrationBuilder.AddForeignKey(
                name: "FK_PetPhotos_PetPost_PetPostPostId",
                table: "PetPhotos",
                column: "PetPostPostId",
                principalTable: "PetPost",
                principalColumn: "PostId",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
