﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Pethelp.Pets.Infrastructure.Migrations
{
    public partial class AddCampos : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "DateOccurrence",
                table: "PetPost",
                type: "datetime(6)",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DateOccurrence",
                table: "PetPost");
        }
    }
}
