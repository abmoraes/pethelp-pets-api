using System;
using System.IO;
using System.Threading.Tasks;
using Amazon.S3;
using Amazon.S3.Model;
using Pethelp.Pets.Domain.Services;

namespace Pethelp.Pets.Infrastructure.Services
{
    public class AwsS3Service : IAwsS3Service
    {
        private readonly IAmazonS3 _awsS3Client;

        public AwsS3Service(IAmazonS3 awsS3Client)
        {
            _awsS3Client = awsS3Client;
        }

        public async Task UploadFileToS3(string bucketName, string fileName, string base64)
        {
            byte[] bytes = Convert.FromBase64String(base64);

            try
            {
                using var newMemoryStream = new MemoryStream(bytes);
                var uploadRequest = new PutObjectRequest
                {
                    InputStream = newMemoryStream,
                    Key = fileName,
                    BucketName = bucketName,
                    CannedACL = S3CannedACL.PublicRead
                };

                await _awsS3Client.PutObjectAsync(uploadRequest);
            }
            catch (AmazonS3Exception e)
            {
                throw new AmazonS3Exception(e.Message);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        public string GenerateUrlAssigned(string bucketName, string fileName, double duration = 1)
        {
            string urlString;
            try
            {
                GetPreSignedUrlRequest request1 = new GetPreSignedUrlRequest
                {
                    BucketName = bucketName,
                    Key = fileName,
                    Expires = DateTime.UtcNow.AddHours(duration)
                };
                urlString = _awsS3Client.GetPreSignedURL(request1);
            }
            catch (AmazonS3Exception e)
            {
                Console.WriteLine("Error encountered on server. Message:'{0}' when writing an object", e.Message);
                throw new Exception("Ocorreu um erro ao realizar o upload das fotos. " + e.Message);
            }
            catch (Exception e)
            {
                Console.WriteLine("Unknown encountered on server. Message:'{0}' when writing an object", e.Message);
                throw new Exception("Ocorreu um erro ao realizar o upload das fotos. " + e.Message);
            }
            return urlString;
        }
    }
}