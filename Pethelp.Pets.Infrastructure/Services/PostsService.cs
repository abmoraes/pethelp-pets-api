using System;
using System.Threading;
using System.Threading.Tasks;
using Pethelp.Pets.Domain.Extentions;
using Pethelp.Pets.Domain.Model;
using Pethelp.Pets.Domain.Model.Enumeradores;
using Pethelp.Pets.Domain.Model.Results;
using Pethelp.Pets.Domain.Repository;
using Pethelp.Pets.Domain.Services;

namespace Pethelp.Pets.Infrastructure.Services
{
    public class PostsService : IPostsService
    {
        private readonly IPetsRepository Repository;

        public PostsService(IPetsRepository repository)
        {
            Repository = repository;
        }

        public async Task<PagedModel<GetPostsByStatusResult>> GetPagedPostsByStatus(int statusId, int page, int limit, CancellationToken cancellationToken)
        {
            var status = Enum.GetName(typeof(PostStatus), statusId);

            var resultPaged = await Repository.GetPagedPostByStatusAsync(status, page, limit, cancellationToken);

            return resultPaged;
        }
    }
}