using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Pethelp.Pets.Domain.Model;

namespace Pethelp.Pets.Infrastructure.Repository
{
    public class PetsDbContext : DbContext
    {
        public DbSet<Pet> Pets { get; set; }
        public DbSet<PetPhotos> PetPhotos { get; set; }
        public DbSet<PetPost> PetPost { get; set; }
        public DbSet<PostAddress> PostAddress { get; set; }

        public PetsDbContext(DbContextOptions<PetsDbContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
        }
    }
}