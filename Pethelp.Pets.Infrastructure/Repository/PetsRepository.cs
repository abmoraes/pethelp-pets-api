using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Pethelp.Pets.Domain.Extentions;
using Pethelp.Pets.Domain.Model;
using Pethelp.Pets.Domain.Model.Results;
using Pethelp.Pets.Domain.Repository;

namespace Pethelp.Pets.Infrastructure.Repository
{
    public class PetsRepository : IPetsRepository
    {
        private PetsDbContext Context { get; set; }

        public PetsRepository(PetsDbContext context)
        {
            Context = context;
        }

        public async Task<PetPost> SavePostAsync(PetPost petPost)
        {
            Context.Entry(petPost).State = EntityState.Added;
            Context.Entry(petPost.Pet).State = EntityState.Added;
            Context.Entry(petPost.PostAddress).State = EntityState.Added;
            // Context.Entry(petPost.PostPhotos).State = EntityState.Added;

            await Context.SaveChangesAsync();

            return petPost;
        }

        public async Task<PetPost> GetPostByIdAsync(int postId)
        {
            var petPost = await Context.PetPost.Include(x => x.Pet)
                                                .Include(x => x.PostAddress)
                                                .Include(x => x.Pet.Photos)
                                                .SingleOrDefaultAsync(x => x.PostId == postId);

            return petPost;
        }

        // public async Task<GetPostsByStatusResult> GetPostByStatusAsync(String Status, CancellationToken cancellationToken)
        // {
        //     var petPost = await Context.PetPost
        //                         .Select(x => new GetPostsByStatusResult { PetName = x.Pet.Name, Status = x.Status, UserId = x.UserId, CreatedAt = x.CreatedAt, UpdatedAt = x.UpdatedAt })
        //                         .Where(x => x.Status == Status)
        //                         .OrderBy(x => x.CreatedAt);

        //     return petPost;
        // }

        public async Task<PagedModel<GetPostsByStatusResult>> GetPagedPostByStatusAsync(String Status, int page, int limit, CancellationToken cancellationToken)
        {
            var petPost = await Context.PetPost
                                .AsNoTracking()
                                .Select(x => new GetPostsByStatusResult
                                {
                                    PetName = x.Pet.Name,
                                    City = x.PostAddress != null ? x.PostAddress.City : "",
                                    Breed = x.Pet.Breed,
                                    Size = x.Pet.Size,
                                    Gender = x.Pet.Gender,
                                    Desc = x.Desc,
                                    Status = x.Status,
                                    UserId = x.UserId,
                                    CreatedAt = x.CreatedAt,
                                    UpdatedAt = x.UpdatedAt
                                })
                                .Where(x => x.Status == Status)
                                .OrderBy(x => x.CreatedAt)
                                .PaginateAsync(page, limit, cancellationToken);

            return petPost;
        }
    }
}