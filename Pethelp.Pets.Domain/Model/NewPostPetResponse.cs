using System;

namespace Pethelp.Pets.Domain.Model
{
    public class NewPostPetResponse
    {
        public int PetId { get; set; }
        public String Name { get; set; }
        public String Type { get; set; }
        public String Breed { get; set; }
        public String Gender { get; set; }
        public DateTime DateBirth { get; set; }
        public String Size { get; set; }
        public String Color { get; set; }
        public bool Castrated { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
    }
}