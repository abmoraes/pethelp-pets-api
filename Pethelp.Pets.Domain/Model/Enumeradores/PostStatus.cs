using System.Text.Json.Serialization;
using Newtonsoft.Json.Converters;

namespace Pethelp.Pets.Domain.Model.Enumeradores
{
    public enum PostStatus
    {
        PET_PERDIDO = 1,
        PET_ENCONTRADO = 2,
        PET_EM_CASA = 3,
        PET_DOACAO = 4,
        PET_ADOTADO = 5,
    }
}