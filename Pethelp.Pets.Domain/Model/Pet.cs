using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Pethelp.Pets.Domain.Model
{
    public class Pet
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int PetId { get; set; }

        [Column(TypeName = "varchar(20)")]
        public String Name { get; set; }

        [Column(TypeName = "varchar(20)")]
        public String Type { get; set; }

        [Column(TypeName = "varchar(50)")]
        public String Breed { get; set; }

        [StringLength(30)]
        public String Gender { get; set; }

        [Column(TypeName = "Date")]
        public DateTime DateBirth { get; set; }

        [Column(TypeName = "varchar(20)")]
        public String Size { get; set; }

        [Column(TypeName = "varchar(20)")]
        public String Color { get; set; }
        public bool Castrated { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
        public List<PetPhotos> Photos { get; set; }

        public bool ShouldSerializePhotos()
        {
            return (Photos != null);
        }
    }
}