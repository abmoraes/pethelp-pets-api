using System.Collections.Generic;

namespace Pethelp.Pets.Domain.Model
{
    public class PostDataModel
    {
        public List<PetPost> Data { get; set; }
    }
}