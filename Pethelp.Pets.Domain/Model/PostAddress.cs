using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Pethelp.Pets.Domain.Model
{
    public class PostAddress
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int AddressId { get; set; }

        [Column(TypeName = "varchar(50)")]
        public String Address { get; set; }

        [Column(TypeName = "varchar(6)")]
        public String Number { get; set; }

        [Column(TypeName = "varchar(50)")]
        public String Neighborhood { get; set; }

        [Column(TypeName = "varchar(50)")]
        public String City { get; set; }

        [Column(TypeName = "varchar(50)")]
        public String State { get; set; }

        [Column(TypeName = "varchar(8)")]
        public String PostalCode { get; set; }

        [Column(TypeName = "varchar(50)")]
        public String Country { get; set; }
    }
}