using System;

namespace Pethelp.Pets.Domain.Model
{
    public class PostModel
    {
        public int PostId { get; set; }
        public Pet Pet { get; set; }
        public String PetName { get; set; }
        public int UserId { get; set; }
        public String Status { get; set; }
        public String Desc { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
    }
}