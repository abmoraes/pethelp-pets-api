using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Pethelp.Pets.Domain.Model.Requests
{
    public class NewPostRequest
    {
        [Required(ErrorMessage = "O nome do pet é obrigatório", AllowEmptyStrings = false)]
        public String Name { get; set; }

        [Required(ErrorMessage = "O tipo do pet é obrigatório", AllowEmptyStrings = false)]
        public String Type { get; set; }

        [Required(ErrorMessage = "A raça do pet é obrigatória", AllowEmptyStrings = false)]
        [StringLength(50)]
        public String Breed { get; set; }

        [Required(ErrorMessage = "O gênero do pet é obrigatório", AllowEmptyStrings = false)]
        [StringLength(30)]
        public String Gender { get; set; }

        [Required(ErrorMessage = "A data de nascimento do pet é obrigatória", AllowEmptyStrings = false)]
        public DateTime DateBirth { get; set; }

        [Required(ErrorMessage = "O tamanho do pet é obrigatório", AllowEmptyStrings = false)]
        [StringLength(20)]
        public String Size { get; set; }

        [Required(ErrorMessage = "A cor do pet é obrigatória", AllowEmptyStrings = false)]
        [StringLength(20)]
        public String Color { get; set; }

        public bool Castrated { get; set; }

        public int UserId { get; set; }

        [Required(ErrorMessage = "O status do post é obrigatório", AllowEmptyStrings = false)]
        [StringLength(50)]
        public String Status { get; set; }

        public String Desc { get; set; }

        [Required(ErrorMessage = "O telefone do responsável para contato é obrigatório", AllowEmptyStrings = false)]
        [StringLength(50)]
        public String Phone { get; set; }

        [Required(ErrorMessage = "A data de ocorrência é obrigatória", AllowEmptyStrings = false)]
        public DateTime DateOccurrence { get; set; }

        public NewPostAddressRequest Address { get; set; }

        public List<NewPostPhotosRequest> Photos { get; set; }
    }
}