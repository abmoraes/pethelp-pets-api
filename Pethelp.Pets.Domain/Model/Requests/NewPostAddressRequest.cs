using System;
using System.ComponentModel.DataAnnotations;

namespace Pethelp.Pets.Domain.Model.Requests
{
    public class NewPostAddressRequest
    {
        [Required(ErrorMessage = "o endereço é obrigatório", AllowEmptyStrings = false)]
        [StringLength(50)]
        public String Address { get; set; }

        [Required(ErrorMessage = "O número do endereço é obrigatório", AllowEmptyStrings = false)]
        [StringLength(6)]
        public String Number { get; set; }

        [Required(ErrorMessage = "O bairro do endereço é obrigatório", AllowEmptyStrings = false)]
        [StringLength(50)]
        public String Neighborhood { get; set; }

        [Required(ErrorMessage = "A cidade do endereço é obrigatória", AllowEmptyStrings = false)]
        [StringLength(50)]
        public String City { get; set; }

        [Required(ErrorMessage = "O estado do endereço é obrigatório", AllowEmptyStrings = false)]
        [StringLength(50)]
        public String State { get; set; }

        [Required(ErrorMessage = "O cep do endereço é obrigatório", AllowEmptyStrings = false)]
        [StringLength(8)]
        public String PostalCode { get; set; }

        [StringLength(50)]
        public String Country { get; set; }

    }
}