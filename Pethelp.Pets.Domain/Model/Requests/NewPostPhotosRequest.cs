using System;
using System.ComponentModel.DataAnnotations;

namespace Pethelp.Pets.Domain.Model.Requests
{
    public class NewPostPhotosRequest
    {
        public String FileName { get; set; }
    }
}