using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Pethelp.Pets.Domain.Model
{
    public class PostPhotos
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int PhotoId { get; set; }
        public String Key { get; set; }

        [Column(TypeName = "varchar(50)")]
        public String BucketName { get; set; }
        public DateTime CreatedAt { get; set; }
    }
}