using System;

namespace Pethelp.Pets.Domain.Model.Results
{
    public class GetPostsByStatusResult
    {
        public String PetName { get; set; }
        public String City { get; set; }
        public String Breed { get; set; }
        public String Size { get; set; }
        public String Gender { get; set; }
        public String Desc { get; set; }
        public String Status { get; set; }
        public int UserId { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
    }
}