using System;
using System.Collections.Generic;

namespace Pethelp.Pets.Domain.Model
{
    public class NewPostResponse
    {
        public int PostId { get; set; }
        public NewPostPetResponse Pet { get; set; }
        public int UserId { get; set; }
        public PostAddress PostAddress { get; set; }
        public string Status { get; set; }
        public string Desc { get; set; }
        public string Phone { get; set; }
        public DateTime DateOccurrence { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
        public List<NewPostPhotosResponse> Photos { get; set; }
    }
}