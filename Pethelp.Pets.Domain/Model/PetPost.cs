using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Pethelp.Pets.Domain.Model
{
    public class PetPost
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int PostId { get; set; }
        public Pet Pet { get; set; }
        public int UserId { get; set; }
        public PostAddress PostAddress { get; set; }

        [Column(TypeName = "varchar(50)")]
        public String Status { get; set; }
        public String Desc { get; set; }

        [Column(TypeName = "varchar(11)")]
        public String Phone { get; set; }
        public DateTime DateOccurrence { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }

        public bool ShouldSerializePostAddress()
        {
            return (PostAddress != null);
        }

        // public bool ShouldSerializePostPhotos()
        // {
        //     return (PostPhotos != null);
        // }
    }
}