namespace Pethelp.Pets.Domain.Model
{
    public class NewPostPhotosResponse
    {
        public string FileName { get; set; }
        public string Url { get; set; }
    }
}