using System.Threading.Tasks;

namespace Pethelp.Pets.Domain.Services
{
    public interface IAwsS3Service
    {
        Task UploadFileToS3(string bucketName, string fileName, string base64);
        string GenerateUrlAssigned(string bucketName, string fileName, double duration = 1);
    }
}