using System.Threading;
using System.Threading.Tasks;
using Pethelp.Pets.Domain.Extentions;
using Pethelp.Pets.Domain.Model;
using Pethelp.Pets.Domain.Model.Results;

namespace Pethelp.Pets.Domain.Services
{
    public interface IPostsService
    {
        Task<PagedModel<GetPostsByStatusResult>> GetPagedPostsByStatus(int status, int page, int limit, CancellationToken cancellationToken);
    }
}