using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.Extensions.Configuration;
using Pethelp.Pets.Domain.Commands;
using Pethelp.Pets.Domain.Model;
using Pethelp.Pets.Domain.Model.Requests;
using Pethelp.Pets.Domain.Repository;
using Pethelp.Pets.Domain.Services;

namespace Pethelp.Pets.Domain.Handlers
{
    public class NewPostHandler : IRequestHandler<NewPostCommand, NewPostResponse>
    {
        private readonly IPetsRepository _repository;
        private readonly IAwsS3Service _awsS3Service;
        private readonly IConfiguration _configuration;

        public NewPostHandler(IPetsRepository repository,
                            IAwsS3Service awsS3Service,
                            IConfiguration configuration)
        {
            _repository = repository;
            _awsS3Service = awsS3Service;
            _configuration = configuration;
        }

        public async Task<NewPostResponse> Handle(NewPostCommand command, CancellationToken cancellationToken)
        {
            List<PetPhotos> petPhotos = new();
            List<NewPostPhotosResponse> newPostPhotosResponse = new();

            var request = command.NewPostRequest;

            var bucketName = _configuration.GetSection("S3:BucketName").Value;



            foreach (var photo in request.Photos)
            {
                var url = _awsS3Service.GenerateUrlAssigned(bucketName, photo.FileName, 1);

                petPhotos.Add(new PetPhotos
                {
                    BucketName = bucketName,
                    Key = photo.FileName,
                    CreatedAt = DateTime.UtcNow
                });

                newPostPhotosResponse.Add(new NewPostPhotosResponse
                {
                    FileName = photo.FileName,
                    Url = url
                });
            }

            PetPost petPost = ConvertRequestToPetPostDb(command.NewPostRequest, petPhotos);

            await _repository.SavePostAsync(petPost);

            return ConvertPetPostToNewPostResponse(petPost, newPostPhotosResponse);
        }

        private static NewPostResponse ConvertPetPostToNewPostResponse(PetPost petPost, List<NewPostPhotosResponse> newPostPhotosResponse)
        {
            var pet = new NewPostPetResponse
            {
                PetId = petPost.Pet.PetId,
                Name = petPost.Pet.Name,
                Type = petPost.Pet.Type,
                Breed = petPost.Pet.Breed,
                Gender = petPost.Pet.Gender,
                DateBirth = petPost.Pet.DateBirth,
                Size = petPost.Pet.Size,
                Color = petPost.Pet.Color,
                Castrated = petPost.Pet.Castrated,
                CreatedAt = petPost.Pet.CreatedAt,
                UpdatedAt = petPost.Pet.UpdatedAt
            };

            return new NewPostResponse
            {
                PostId = petPost.PostId,
                Pet = pet,
                UserId = petPost.UserId,
                PostAddress = petPost.PostAddress,
                Status = petPost.Status,
                Desc = petPost.Desc,
                Phone = petPost.Phone,
                DateOccurrence = petPost.DateOccurrence,
                CreatedAt = petPost.CreatedAt,
                UpdatedAt = petPost.UpdatedAt,
                Photos = newPostPhotosResponse
            };
        }

        private static PetPost ConvertRequestToPetPostDb(NewPostRequest newPostRequest, List<PetPhotos> petPhotos = null)
        {
            PostAddress address = null;

            if (newPostRequest.Address is not null)
            {
                address = new PostAddress
                {
                    Address = newPostRequest.Address?.Address,
                    Number = newPostRequest.Address?.Number,
                    Neighborhood = newPostRequest.Address?.Neighborhood,
                    City = newPostRequest.Address?.City,
                    State = newPostRequest.Address?.State,
                    PostalCode = newPostRequest.Address?.PostalCode,
                    Country = newPostRequest.Address?.Country
                };
            }

            PetPost petPost = new()
            {
                Pet = new Pet
                {
                    Name = newPostRequest.Name,
                    Type = newPostRequest.Type,
                    Breed = newPostRequest.Breed,
                    Gender = newPostRequest.Gender,
                    DateBirth = newPostRequest.DateBirth,
                    Size = newPostRequest.Size,
                    Color = newPostRequest.Color,
                    Castrated = newPostRequest.Castrated,
                    CreatedAt = DateTime.Now,
                    UpdatedAt = DateTime.Now,
                    Photos = petPhotos
                },
                UserId = newPostRequest.UserId,
                PostAddress = address,
                Status = newPostRequest.Status,
                Desc = newPostRequest.Desc,
                Phone = newPostRequest.Phone,
                DateOccurrence = newPostRequest.DateOccurrence,
                CreatedAt = DateTime.Now,
                UpdatedAt = DateTime.Now
            };

            return petPost;
        }
    }
}