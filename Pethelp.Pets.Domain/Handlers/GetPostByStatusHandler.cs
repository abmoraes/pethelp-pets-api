using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Pethelp.Pets.Domain.Commands;
using Pethelp.Pets.Domain.Model;
using Pethelp.Pets.Domain.Repository;
using System;
using Pethelp.Pets.Domain.Model.Enumeradores;
using System.Linq;
using Pethelp.Pets.Domain.Extentions;
using Microsoft.Extensions.Logging;
using Pethelp.Pets.Domain.Services;
using Pethelp.Pets.Domain.Model.Results;

namespace Pethelp.Pets.Domain.Handlers
{
    public class GetPostByStatusHandler : IRequestHandler<GetPostByStatusCommand, PagedModel<GetPostsByStatusResult>>
    {
        private readonly IPostsService _service;
        private readonly ILogger<GetPostByStatusHandler> _logger;

        public GetPostByStatusHandler(IPostsService service, ILogger<GetPostByStatusHandler> logger)
        {
            _service = service;
            _logger = logger;
        }

        public async Task<PagedModel<GetPostsByStatusResult>> Handle(GetPostByStatusCommand command, CancellationToken cancellationToken)
        {
            try
            {
                return await _service.GetPagedPostsByStatus(status: command.statusId, page: command.page, limit: command.limit, cancellationToken: cancellationToken);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Erro ao retornar Posts para Status {command.statusId}. Detalhes: " + ex.Message);
                throw;
            }
        }
    }
}