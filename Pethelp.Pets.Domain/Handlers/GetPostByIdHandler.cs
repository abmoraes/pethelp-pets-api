using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Pethelp.Pets.Domain.Commands;
using Pethelp.Pets.Domain.Model;
using Pethelp.Pets.Domain.Repository;

namespace Pethelp.Pets.Domain.Handlers
{
    public class GetPostByIdHandler : IRequestHandler<GetPostByIdCommand, PetPost>
    {
        private readonly IPetsRepository _repository;

        public GetPostByIdHandler(IPetsRepository repository)
        {
            _repository = repository;
        }

        public async Task<PetPost> Handle(GetPostByIdCommand command, CancellationToken cancellationToken)
        {
            var result = await _repository.GetPostByIdAsync(command.postId);
            return result;
        }
    }
}