using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Pethelp.Pets.Domain.Commands;
using Pethelp.Pets.Domain.Model;
using Pethelp.Pets.Domain.Repository;
using System;
using Pethelp.Pets.Domain.Model.Enumeradores;
using Newtonsoft.Json;
using System.Linq;
using System.Collections.Generic;

namespace Pethelp.Pets.Domain.Handlers
{
    public class GetPostStatusHandler : IRequestHandler<GetPostStatusCommand, Dictionary<int, string>>
    {
        public GetPostStatusHandler()
        {
        }

        public Task<Dictionary<int, string>> Handle(GetPostStatusCommand command, CancellationToken cancellationToken)
        {
            return Task.FromResult(Enum.GetValues(typeof(PostStatus)).Cast<PostStatus>()
                             .ToDictionary(i => (int)Convert.ChangeType(i, i.GetType()), t => t.ToString()));
        }
    }
}