using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Pethelp.Pets.Domain.Extentions;
using Pethelp.Pets.Domain.Model;
using Pethelp.Pets.Domain.Model.Results;

namespace Pethelp.Pets.Domain.Repository
{
    public interface IPetsRepository
    {
        Task<PetPost> SavePostAsync(PetPost petPost);
        Task<PetPost> GetPostByIdAsync(int PostId);
        Task<PagedModel<GetPostsByStatusResult>> GetPagedPostByStatusAsync(String Status, int page, int limit, CancellationToken cancellationToken);
    }
}