using System.Collections.Generic;
using MediatR;
using Pethelp.Pets.Domain.Extentions;
using Pethelp.Pets.Domain.Model;
using Pethelp.Pets.Domain.Model.Results;

namespace Pethelp.Pets.Domain.Commands
{
    public class GetPostByStatusCommand : IRequest<PagedModel<GetPostsByStatusResult>>
    {
        public int statusId;
        public int page;
        public int limit;
        public string authorization;

        public GetPostByStatusCommand(int statusId, int page, int limit, string authorization)
        {
            this.statusId = statusId;
            this.page = page;
            this.limit = limit;
            this.authorization = authorization;
        }
    }
}