using MediatR;
using Pethelp.Pets.Domain.Model;
using Pethelp.Pets.Domain.Model.Requests;

namespace Pethelp.Pets.Domain.Commands
{
    public class NewPostCommand : IRequest<NewPostResponse>
    {
        public NewPostRequest NewPostRequest { get; set; }
        public string Authorization { get; set; }

        public NewPostCommand(NewPostRequest newPostRequest, string authorization)
        {
            this.NewPostRequest = newPostRequest;
            this.Authorization = authorization;
        }
    }
}