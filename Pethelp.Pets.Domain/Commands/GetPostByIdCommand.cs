using MediatR;
using Pethelp.Pets.Domain.Model;

namespace Pethelp.Pets.Domain.Commands
{
    public class GetPostByIdCommand : IRequest<PetPost>
    {
        public int postId;
        public string authorization;

        public GetPostByIdCommand(int postId, string authorization)
        {
            this.postId = postId;
            this.authorization = authorization;
        }
    }
}