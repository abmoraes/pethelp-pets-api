using System.Collections.Generic;
using MediatR;
using Pethelp.Pets.Domain.Model;

namespace Pethelp.Pets.Domain.Commands
{
    public class GetPostStatusCommand : IRequest<Dictionary<int, string>>
    {
        public string authorization;

        public GetPostStatusCommand(string authorization)
        {
            this.authorization = authorization;
        }
    }
}