using System.Collections.Generic;

namespace Pethelp.Pets.Domain.Extentions
{
    public class PagedModel<TModel> where TModel : class
    {
        public int CurrentPage { get; internal set; }
        public int PageSize { get; internal set; }
        public List<TModel> Items { get; internal set; }
        public int TotalItems { get; internal set; }
        public int TotalPages { get; internal set; }
    }
}