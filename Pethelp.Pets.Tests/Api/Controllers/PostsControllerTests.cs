using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Pethelp.Pets.Api.Controllers;
using Pethelp.Pets.Domain.Commands;
using Pethelp.Pets.Domain.Model;
using Xunit;

namespace Pethelp.Pets.Tests.Api.Controllers
{
    public class PostsControllerTests
    {
        public Mock<IMediator> _mediator;
        public PostsController _controller;
        private NewPostCommand _command;

        public PostsControllerTests()
        {
            _mediator = new Mock<IMediator>();
        }

        [Fact]
        public async Task ShouldNewPost_ReturnsPetPostWithSuccess()
        {
            var expectedResult = TestHelper.PetPostResult();
            _command = new NewPostCommand(TestHelper.NewPostRequest(), "123");

            _mediator.Setup(x => x.Send(It.IsAny<NewPostCommand>(), CancellationToken.None)).ReturnsAsync(expectedResult);

            _controller = new PostsController(_mediator.Object);
            var result = await _controller.NewPost(TestHelper.NewPostRequest());

            var objectResult = result as CreatedResult;

            Assert.NotNull(objectResult);

            var petPostResult = objectResult.Value as PetPost;


            Assert.Equal(expectedResult.Pet.Name, petPostResult.Pet.Name);
            Assert.Equal(expectedResult.Pet.Breed, petPostResult.Pet.Breed);
            Assert.Equal(expectedResult.UserId, petPostResult.UserId);

            Assert.NotEqual(0, petPostResult.Pet.PetId);
            Assert.NotEqual(0, petPostResult.PostId);

            Assert.Equal(expectedResult.PostAddress.Address, petPostResult.PostAddress.Address);
        }
    }
}