using System;
using Pethelp.Pets.Domain.Model;
using Pethelp.Pets.Domain.Model.Requests;

namespace Pethelp.Pets.Tests
{
    public class TestHelper
    {
        private const int _petPostUserId = 1;
        private const int _postId = 1;
        private const int _petPostId = 1;
        private const int _petPostAddressId = 1;
        private const string _petPostName = "Teste";
        private const string _petPostType = "Teste";
        private const string _petPostBreed = "Teste";
        public const string _petPostDateBirth = "04/12/2020";
        private const string _petPostSize = "Teste";
        private const string _petPostColor = "Teste";
        private const bool _petPostCastrated = true;
        private const string _petPostAddress = "Teste";
        private const string _petPostAddressNumber = "Teste";
        private const string _petPostAddressNeighborhood = "Teste";
        private const string _petPostAddressCity = "Teste";
        private const string _petPostAddressState = "Teste";
        private const string _petPostAddressPostalCode = "Teste";
        private const string _petPosAddressCountry = "Teste";
        private const string _petPostStatus = "Teste";
        private const string _petPostDesc = "Teste";
        private const string _petPostPhone = "Teste";

        public static int PetPostUserId { get { return _petPostUserId; } }
        public static int PostId { get { return _postId; } }
        public static int PetPostId { get { return _petPostId; } }
        public static int PetPostAddressId { get { return _petPostAddressId; } }
        public static string PetPostName { get { return _petPostName; } }
        public static string PetPostType { get { return _petPostType; } }
        public static string PetPostBreed { get { return _petPostBreed; } }
        public static string PetPostDateBirth { get { return _petPostDateBirth; } }
        public static string PetPostSize { get { return _petPostSize; } }
        public static string PetPostColor { get { return _petPostColor; } }
        public static bool PetPostCastrated { get { return _petPostCastrated; } }
        public static string PetPostAddress { get { return _petPostAddress; } }
        public static string PetPostAddressNumber { get { return _petPostAddressNumber; } }
        public static string PetPostAddressNeighborhood { get { return _petPostAddressNeighborhood; } }
        public static string PetPostAddressCity { get { return _petPostAddressCity; } }
        public static string PetPostAddressState { get { return _petPostAddressState; } }
        public static string PetPostAddressPostalCode { get { return _petPostAddressPostalCode; } }
        public static string PetPostAddressCountry { get { return _petPosAddressCountry; } }
        public static string PetPostStatus { get { return _petPostStatus; } }
        public static string PetPostDesc { get { return _petPostDesc; } }
        public static string PetPostPhone { get { return _petPostPhone; } }

        public static NewPostRequest NewPostRequest()
        {
            return new NewPostRequest
            {

                Name = PetPostName,
                Type = PetPostType,
                Breed = PetPostBreed,
                DateBirth = DateTime.Parse(PetPostDateBirth),
                Size = PetPostSize,
                Color = PetPostColor,
                Castrated = PetPostCastrated,
                UserId = PetPostUserId,
                Address = new NewPostAddressRequest
                {
                    Address = PetPostAddress,
                    Number = PetPostAddressNumber,
                    Neighborhood = PetPostAddressNeighborhood,
                    City = PetPostAddressCity,
                    State = PetPostAddressState,
                    PostalCode = PetPostAddressPostalCode,
                    Country = PetPostAddressCountry
                },
                Status = PetPostStatus,
                Desc = PetPostDesc,
                Phone = PetPostPhone
            };
        }

        public static PetPost PetPostRequest()
        {
            return new PetPost
            {
                Pet = new Pet
                {
                    Name = PetPostName,
                    Type = PetPostType,
                    Breed = PetPostBreed,
                    DateBirth = DateTime.Parse(PetPostDateBirth),
                    Size = PetPostSize,
                    Color = PetPostColor,
                    Castrated = PetPostCastrated,
                    CreatedAt = DateTime.Now,
                    UpdatedAt = DateTime.Now
                },
                UserId = PetPostUserId,
                PostAddress = new PostAddress
                {
                    Address = PetPostAddress,
                    Number = PetPostAddressNumber,
                    Neighborhood = PetPostAddressNeighborhood,
                    City = PetPostAddressCity,
                    State = PetPostAddressState,
                    PostalCode = PetPostAddressPostalCode,
                    Country = PetPostAddressCountry
                },
                Status = PetPostStatus,
                Desc = PetPostDesc,
                Phone = PetPostPhone,
                CreatedAt = DateTime.Now,
                UpdatedAt = DateTime.Now
            };
        }

        public static PetPost PetPostResult()
        {
            return new PetPost
            {
                PostId = PostId,
                Pet = new Pet
                {
                    PetId = PetPostId,
                    Name = PetPostName,
                    Type = PetPostType,
                    Breed = PetPostBreed,
                    DateBirth = DateTime.Parse(PetPostDateBirth),
                    Size = PetPostSize,
                    Color = PetPostColor,
                    Castrated = PetPostCastrated,
                    CreatedAt = DateTime.Now,
                    UpdatedAt = DateTime.Now
                },
                UserId = PetPostUserId,
                PostAddress = new PostAddress
                {
                    AddressId = PetPostAddressId,
                    Address = PetPostAddress,
                    Number = PetPostAddressNumber,
                    Neighborhood = PetPostAddressNeighborhood,
                    City = PetPostAddressCity,
                    State = PetPostAddressState,
                    PostalCode = PetPostAddressPostalCode,
                    Country = PetPostAddressCountry
                },
                Status = PetPostStatus,
                Desc = PetPostDesc,
                Phone = PetPostPhone,
                CreatedAt = DateTime.Now,
                UpdatedAt = DateTime.Now
            };
        }
    }
}