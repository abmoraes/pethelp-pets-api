// using Microsoft.EntityFrameworkCore;
// using Xunit;
// using Moq;
// using System.Threading;
// using Pethelp.Pets.Domain.Repository;
// using Pethelp.Pets.Domain.Model;
// using Pethelp.Pets.Infrastructure.Repository;
// using Microsoft.Extensions.DependencyInjection;
// using Microsoft.EntityFrameworkCore.ChangeTracking;

// namespace Pethelp.Pets.Tests.Infrastructure
// {
//     public class PetRepositoryTests
//     {
//         public Mock<PetsDbContext> Context;
//         public Mock<DbContextOptions<PetsDbContext>> ContextOptions;

//         public IPetsRepository Repository;

//         public PetRepositoryTests()
//         {
//             ContextOptions = new Mock<DbContextOptions<PetsDbContext>>();
//             Context = new Mock<PetsDbContext>(ContextOptions.Object);
//         }

//         [Fact]
//         public void Should_SavePostAsync_ReturnsAPost()
//         {
//             PetPost petPost = TestHelper.PetPostRequest();


//             // var entity = new EntityEntry<PetPost>;

//             // Context.Setup(c=>c.DbSet)

//             // Context.Setup(x => x.Entry(petPost)).Returns(new EntityEntry<PetPost>());
//             // Context.Setup(x => x.Entry(petPost.Pet));
//             // Context.Setup(x => x.Entry(petPost.PostAddress));
//             // Context.Entry(petPost).State = EntityState.Added;
//             // Context.Entry(petPost.Pet).State = EntityState.Added;
//             // Context.Entry(petPost.PostAddress).State = EntityState.Added;

//             Context.Setup(x => x.SaveChangesAsync(CancellationToken.None));

//             Repository = new PetsRepository(Context.Object);

//             Repository.SavePostAsync(petPost);

//             Context.Verify(x => x.SaveChangesAsync(CancellationToken.None), Times.Once);
//         }
//     }
// }