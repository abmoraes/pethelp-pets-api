using System.Threading;
using System.Threading.Tasks;
using Moq;
using Pethelp.Pets.Domain.Commands;
using Pethelp.Pets.Domain.Handlers;
using Pethelp.Pets.Domain.Model;
using Pethelp.Pets.Domain.Model.Requests;
using Pethelp.Pets.Domain.Repository;
using Xunit;

namespace Pethelp.Pets.Tests.Domain.Handlers
{
    public class NewPostHandlerTests
    {
        public Mock<IPetsRepository> _repository;
        private NewPostHandler _handler;
        private NewPostCommand _command;

        public NewPostHandlerTests()
        {
            _repository = new Mock<IPetsRepository>();
        }

        [Fact]
        public async Task ShouldHandle_ReturnsPetPostWithSuccess()
        {
            var expectedResult = TestHelper.PetPostResult();
            _command = new NewPostCommand(TestHelper.NewPostRequest(), "123");

            _repository.Setup(x => x.SavePostAsync(It.IsAny<PetPost>())).ReturnsAsync(expectedResult);

            _handler = new NewPostHandler(_repository.Object);
            var result = await _handler.Handle(_command, CancellationToken.None);

            Assert.NotNull(result);

            Assert.Equal(expectedResult.Pet.Name, result.Pet.Name);
            Assert.Equal(expectedResult.Pet.Breed, result.Pet.Breed);
            Assert.Equal(expectedResult.UserId, result.UserId);

            Assert.NotEqual(0, result.Pet.PetId);
            Assert.NotEqual(0, result.PostId);

            Assert.Equal(expectedResult.PostAddress.Address, result.PostAddress.Address);
        }

    }
}