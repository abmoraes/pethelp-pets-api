﻿using System.Collections.Generic;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Pethelp.Pets.Domain.Commands;
using Pethelp.Pets.Domain.Extentions;
using Pethelp.Pets.Domain.Model;
using Pethelp.Pets.Domain.Model.Requests;
using Pethelp.Pets.Domain.Model.Results;

namespace Pethelp.Pets.Api.Controllers
{
    [ApiController]
    [Route("/api/[controller]")]
    public class PostsController : ControllerBase
    {
        [FromHeader]
        public string Authorization { get; set; }
        private readonly IMediator _mediator;

        public PostsController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpPost]
        [ProducesResponseType(typeof(PetPost), StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> NewPost([FromBody] NewPostRequest request)
        {
            NewPostCommand command = new(request, Authorization);

            var result = await _mediator.Send(command);

            return Created("GetPost", result);
        }

        [HttpGet("{postId}")]
        [ProducesResponseType(typeof(PetPost), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> GetPostById(int postId)
        {
            GetPostByIdCommand command = new(postId, Authorization);

            var result = await _mediator.Send(command);

            if (result == null)
                return NotFound("Post não foi encontrado na base.");

            return Ok(result);
        }

        [HttpGet("status/{statusId}")]
        [ProducesResponseType(typeof(PagedModel<GetPostsByStatusResult>), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> GetPostByStatus(int statusId, [FromQuery] int page, [FromQuery] int limit)
        {
            GetPostByStatusCommand command = new(statusId, page, limit, Authorization);

            var result = await _mediator.Send(command);

            if (result == null)
                return NotFound("Não foram encontrados posts com esse status.");

            return Ok(result);
        }

        [HttpGet("status")]
        [ProducesResponseType(typeof(Dictionary<int, string>), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> GetPostStatus()
        {
            GetPostStatusCommand command = new(Authorization);

            var result = await _mediator.Send(command);

            if (result == null)
                return NotFound("Não foram encontrados status cadastrados.");

            return Ok(result);
        }
    }
}
